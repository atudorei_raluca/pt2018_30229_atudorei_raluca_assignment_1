package model;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Monom {

	private static final Pattern patternCoef = Pattern.compile("([-,+]?[0-9]*)(.*)");
	private static final Pattern patternOperatorOnly = Pattern.compile("^[-,+]{1}$");
	private static final Pattern patternNec = Pattern.compile("(.*)([x]{1})(.*)");
	private static final Pattern patternPow = Pattern.compile("(.*)([\\^]{1}[0-9]*)$");
	private Double coef;
	private Integer pow;
	private String nec;

	public Monom(Double coef, String nec, Integer pow) {
		this.coef = coef;
		this.pow = pow;
		this.nec = nec;
	}

	public Monom(String input) throws IOException {
		this.prelucrareInput(input);
	}

	public void prelucrareInput(String input) throws IOException {

		Matcher matchCoef = Monom.patternCoef.matcher(input);
		Matcher matchNec = Monom.patternNec.matcher(input);
		Matcher matchPow = Monom.patternPow.matcher(input);
		Matcher matchOperatorOnly = Monom.patternOperatorOnly.matcher(input);

		boolean isCoefMatching = matchCoef.matches();
		boolean isNecMatching = matchNec.matches();
		boolean isPowMatching = matchPow.matches();
		boolean isOperatorOnly = matchOperatorOnly.matches();
		if (!isCoefMatching || isOperatorOnly) {
			throw new IOException(" reincercati");
		}
		//se arunca o exceptie daca se introduc monoame de forma "25x7", lipseste "^"
		if (isNecMatching)
		{
			if(!isPowMatching && matchCoef.end(1)== input.length()) {
				throw new IOException("Putere invalida");
			}
		} 

		if (isCoefMatching) {
			this.setCoef(matchCoef.group(1));
		} else {
			this.setCoef("");
		}
		if (isNecMatching) {
			this.setNec(matchNec.group(2).toString());
		} else {
			this.setNec("");
		}
		if (isPowMatching) {
			this.setPow(matchPow.group(2).toString());
		} else {
			this.setPow("");
		}

	}

	public double getCoef() {
		return this.coef;
	}

	public void setCoef(String coef) {
		if (Monom.patternOperatorOnly.matcher(coef).matches()) {
			coef += "1";
		} else if ("".equals(coef)) {
			coef += "1";
		}
		this.coef = Double.parseDouble(coef);
	}

	public int getPow() {
		return this.pow;
	}

	public void setPow(String pow) {
		if ("".equals(this.nec) && "".equals(pow)) {
			this.pow = 0;
		} else if ("".equals(pow)) {
			this.pow = 1;
		} else {
			this.pow = Integer.parseInt(pow.substring(1));
		}
	}

	public String getNec() {
		return this.nec;
	}

	public void setNec(String nec) {
		this.nec = nec;
	}

	private String getNecString() {
		if (this.pow != 0) {
			return nec;
		}
		return "";
	}

	private String getPowString() {
		if (this.pow != 0 && this.pow != 1 && !"".equals(this.nec)) {
			return "^" + this.pow;
		}
		return "";
	}

	private String getCoefString() {
		if (this.coef>1 && !"".equals(this.nec)) {
			return "+" + this.coef;
		} else if (this.coef > 0 && "".equals(this.nec)) {
			return "+" + this.coef;
		} else if (this.coef == 0) {
			return "";
		} else if(this.coef==1 && !"".equals(this.nec)) {
			return "+";
		}else if(this.coef==-1 && !"".equals(this.nec)) {
			return "-";
		}
		else {
			return "" + coef;
		}
	}

	@Override
	public String toString() {
		return this.getCoefString() + this.getNecString() + this.getPowString();
	}

	public void adunaMonom(Monom m) {
		if (this.pow == m.getPow()) {
			this.coef += m.getCoef();
		}
		if(coef==0) {
			this.nec = "";
			this.pow = 0;
		}

	}

	public void schimbaSemnul() {
		this.coef = -this.coef;
	}

	public void inmultesteMonom(Monom m) {
		this.coef *= m.getCoef();
		this.pow += m.getPow();
		if("".equals(this.nec) && this.pow>=1) {
			this.nec = m.getNec();
		}

	}
	public void deriveazaMonom() {
		if(this.pow == 1) {
			this.nec="";
			this.pow = 0;
		}
		else if(this.pow == 0 || "".equals(this.nec)) {
			this.coef = 0.0;
		}
		else {
			this.coef *= this.pow;
			this.pow -= 1;
		}
	}

	public void integreazaMonom() {
		if("".equals(this.nec)) {
			this.nec= "x";
			this.pow = 1;
		}
		else {
			this.coef= this.coef / (this.pow+1);
			this.pow+= 1;
		}
	}
	public void imparteMonoame(Monom m, Monom n) {
		this.coef= m.getCoef()/n.getCoef();
		this.pow= m.getPow()-n.getPow();
	}

	public void inmultesteMonoame(Monom m, Monom n) {
		this.coef= m.getCoef()*n.getCoef();
		this.pow= m.getPow()+n.getPow();
		if("".equals(this.nec) && this.pow>=1) {
			this.nec = m.getNec();
		}
	}
}
