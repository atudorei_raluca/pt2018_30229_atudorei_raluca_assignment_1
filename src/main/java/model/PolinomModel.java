package model;

import java.io.IOException;

public class PolinomModel implements Model {

	private Polinom firstPolinom;
	private Polinom secondPolinom;

	public PolinomModel() {
		this.reset();
	}
	
	@Override
	public void reset() {
		this.firstPolinom = new Polinom();
		this.secondPolinom = new Polinom();
	}

	@Override
	public void setFirstValue(String value) throws IOException {
		this.firstPolinom = new Polinom(value);
	}

	@Override
	public String getFirstValue() {
		return this.getFirstValue().toString();
	}

	@Override
	public void setSecondValue(String value) throws IOException {
		this.secondPolinom = new Polinom(value);
	}

	@Override
	public String getSecondValue() {
		return this.getSecondValue().toString();
	}

	@Override
	public String sum() {
		Polinom pAdunat = new Polinom(firstPolinom.getPolinom());
		pAdunat.adunaPolinom(secondPolinom);
		return pAdunat.toString();
	}

	@Override
	public String dif() {
		Polinom pScazut = new Polinom(firstPolinom.getPolinom());
		pScazut.scadePolinom(secondPolinom);
		return pScazut.toString();
	}

	@Override
	public String subCat() {
		Polinom pImpartit= new Polinom(firstPolinom.getPolinom());
		pImpartit.impartePolinom(secondPolinom);
		return pImpartit.getCat();
	}
	@Override
	public String subRest() {
		Polinom pImpartit= new Polinom(firstPolinom.getPolinom());
		pImpartit.impartePolinom(secondPolinom);
		return pImpartit.getRest();
	}

	@Override
	public String mult() {
		Polinom pInmultit = new Polinom(firstPolinom.getPolinom());
		pInmultit.inmultestePolinom(secondPolinom);
		return pInmultit.toString();
	}

	@Override
	public String derivFirst() {
		Polinom pDerivat = new Polinom(firstPolinom.getPolinom());
		pDerivat.deriveazaPolinom();
		return pDerivat.toString();
	}

	@Override
	public String derivSecond() {
		Polinom pDerivat = new Polinom(secondPolinom.getPolinom());
		pDerivat.deriveazaPolinom();
		return pDerivat.toString();
	}

	@Override
	public String integrFirst() {
		Polinom pIntegrat = new Polinom(firstPolinom.getPolinom());
		pIntegrat.integreazaPolinom();
		return pIntegrat.toString();
	}

	@Override
	public String integrSecond() {
		Polinom pIntegrat = new Polinom(secondPolinom.getPolinom());
		pIntegrat.integreazaPolinom();
		return pIntegrat.toString();
	}

}
