package model;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Polinom {
	private static final Pattern POLINOM_PATERN = Pattern.compile("([-,+]?[0-9]*[x]?[\\^]?[0-9]*)(.*)");
	private List<Monom> polinom;
	private List<Monom> polinomCat  = new ArrayList<>();
	private List<Monom> polinomRest  = new ArrayList<>();
	private int grad;

	public Polinom() {
		this.setPolinom(new ArrayList<Monom>());
		this.setGrad(0);
	}

	public Polinom(List<Monom> monomlist) {
		this.setPolinom(monomlist);
		this.calculateGrad();
	}

	public Polinom(String value) throws IOException {
		this();
		String str = new String(value);
		while (!"".equals(str)) {
			Matcher monomMatcher = Polinom.POLINOM_PATERN.matcher(str);
			if (monomMatcher.matches()) {
				String monomString = monomMatcher.group(1);
				getPolinom().add(new Monom(monomString));
				str = str.substring(monomString.length());
			} else {
				throw new IOException("Polinom invalid: ");
			}
		}
		this.calculateGrad();
	}
	

	public List<Monom> getMonoame(int grad) {
		List<Monom> monoame = new ArrayList<>();
		for (Monom m : getPolinom()) {
			if (grad == m.getPow()) {
				monoame.add(m);
			}
		}
		return monoame;
	}

	private void calculateGrad() {
		for (Monom m : getPolinom()) {
			if (this.getGrad() < m.getPow()) {
				this.setGrad(m.getPow());
			}
		}
	}

	public void adunaPolinom(Polinom p) {
		this.polinom.addAll(p.getPolinom());
		this.calculateGrad();
		for (int i = this.grad; i >= 0; i--) {
			List<Monom> listMonoame = this.getMonoame(i);
			if (listMonoame.size() != 0) {
				this.polinom.removeAll(listMonoame);
				Monom m = listMonoame.get(0);
				listMonoame.remove(0);
				for (Monom mCurrent : listMonoame) {
					m.adunaMonom(mCurrent);
				}
				this.polinom.add(m);
			}
		}
	}

	public void scadePolinom(Polinom p) {
		for (Monom m : p.getPolinom()) {
			m.schimbaSemnul();
		}
		this.adunaPolinom(p);
	}

	public void inmultestePolinom(Polinom p) {
		List<Monom> polinomInmultit = new ArrayList<>();
		for(Monom m: this.polinom) {
			for(Monom n: p.getPolinom()) {
				Monom inmultit = new Monom(m.getCoef(), m.getNec(), m.getPow());
				inmultit.inmultesteMonom(n);
				polinomInmultit.add(inmultit);
			}
		}
		this.polinom = polinomInmultit;
		this.adunaPolinom(new Polinom());
	}
	
	public void impartePolinom(Polinom p) {
		int gradDeimpartit,gradImpartitor;
		List<Monom> polinomInmultit  = new ArrayList<>();
		Monom rest, impartitor;
		gradDeimpartit= this.polinom.get(0).getPow();
		gradImpartitor= p.getPolinom().get(0).getPow();
		impartitor= p.getMonomGradMax();
		if(gradDeimpartit>=gradImpartitor){
			int gradRest=gradDeimpartit;
			while(gradImpartitor-1 < gradRest) {
				rest=this.polinom.get(0);
				Monom cat= new Monom(0.0,"x",0);
				cat.imparteMonoame(rest, impartitor);
				polinomCat.add(cat);
				for(Monom m: p.getPolinom()) {
					Monom inmultit = new Monom(m.getCoef(), m.getNec(), m.getPow());
					inmultit.inmultesteMonom(cat);
					polinomInmultit.add(inmultit);
				}
				for(Monom m: polinomInmultit) {
					m.schimbaSemnul();
				}
				this.polinom.addAll(polinomInmultit);
				for (int i = gradRest; i >= 0; i--) {
					List<Monom> listMonoame = this.getMonoame(i);
					if (listMonoame.size() != 0) {
						this.polinom.removeAll(listMonoame);
						Monom m = listMonoame.get(0);
						listMonoame.remove(0);
						for (Monom mCurrent : listMonoame) {
							m.adunaMonom(mCurrent);
						}
						this.polinom.add(m);
					}
				}
				gradRest=this.polinom.get(0).getPow();
				polinomInmultit.clear();
			}
		}
	}
	public String getRest() {
		String s="";
		for(Monom m: this.polinom) {
			s+= m;
		}
		return s;
	}
	public String getCat() {
		String s="";
		for(Monom m: polinomCat) {
			s+= m;
		}
		return s;
	}
	
	public void deriveazaPolinom() {
		for(Monom m: this.polinom) {
			m.deriveazaMonom();
		}
	}
	public void integreazaPolinom() {
		for(Monom m: this.polinom) {
			m.integreazaMonom();
		}
	}
	@Override
	public String toString() {
		String s = "";
		for (Monom m : getPolinom()) {
			s += m;
		}
		return s;
	}

	public int getGrad() {
		return grad;
	}

	public void setGrad(int grad) {
		this.grad = grad;
	}

	public List<Monom> getPolinom() {
		return this.polinom;
	}

	public void setPolinom(List<Monom> polinom) {
		this.polinom = polinom;
	}
	public Monom getMonomGradMax() {
		int max=0;
		Monom n= new Monom(0.0,"x",0);
		for(Monom m: this.polinom) {
			if(max < m.getPow()) {
					max= m.getPow();
				}
			}
		for(Monom m: this.polinom) {
			if(m.getPow()==max) {
				n=m;
			}
		}
		return n;
	}	
}
