package model;

import java.io.IOException;

public interface Model {

	 public void setFirstValue(String value) throws IOException;
	 public String getFirstValue();
	 public void setSecondValue(String value) throws IOException;
	 public String getSecondValue();
	 public String sum();
	 public String dif();
	 public String subCat();
	 public String subRest();
	 public String mult();
	 public String derivFirst();
	 public String derivSecond();
	 public String integrFirst();
	 public String integrSecond();
	 public void reset();
}
