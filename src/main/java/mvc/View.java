package mvc;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import model.Model; 
public class View extends JFrame{
		//elementele interfetei:
		/* 2 textField-uri -> corespunzatoare fiecarui polinom
		 * 2 butoane de inregistrare si validare a polinoamelor; in cazul introducerii unui polinom invalid utilizatorul este atentionat
		 * 6 butoane corespunzatoare operatiilor efectuate pe polinoame
		 * 2 textField-uri pentru rezultate: 
		 * 			-> primul textField de rezultat e rezervat pentru catul impartirii sau rezultatul derivarii/integrarii primului polinom
		 * 			-> al doilea textField de rezultat e rezervat pentru restulimpartirii sau rezultatul derivarii/integrarii celui de-al doilea polinom
		 */
		private JTextField tfPolinom1 = new JTextField();
		private JTextField tfPolinom2 = new JTextField();
		private JButton btnInregistrare1= new JButton("Inregistreaza");
		private JButton btnInregistrare2= new JButton("Inregistreaza");
		private JButton btnAdunare = new JButton("+");
		private JButton btnScadere = new JButton("-");
		private JButton btnInmultire = new JButton("*");
		private JButton btnImpartire = new JButton("/");
		private JButton btnDerivare = new JButton("Derivare");
		private JButton btnIntegrare = new JButton("Integrare");
		private JButton btnReset = new JButton("Resetare");
		private JTextField tfRezultat1= new JTextField();
		private JTextField tfRezultat2= new JTextField();
		
		public View() {
			initUI();
		}
		
		public void showDialog(String message) {
			JOptionPane.showMessageDialog(this, message);
		}
		
		public void initUI() {
			 JFrame frame = new JFrame ("Tema 1"); 
			 frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			 frame.setSize(550,350); 
			 JPanel panel1 = new JPanel();
			 JPanel panel2 = new JPanel();
			 JPanel panel3 = new JPanel();
			 JPanel panel4 = new JPanel();
			 JPanel panel5 = new JPanel();

			 tfPolinom1.setPreferredSize(new Dimension(200,24));
			 tfPolinom2.setPreferredSize(new Dimension(200,24));
			 
			 panel1.add(new JLabel("Polinom: "));
			 panel1.add(tfPolinom1);
			 panel1.add(btnInregistrare1);
			 panel2.setLayout(new FlowLayout());
			 panel2.add(new JLabel("Polinom: "));
			 panel2.add(tfPolinom2);
			 panel2.add(btnInregistrare2);
			 panel2.setLayout(new FlowLayout());
	
			 panel3.add(btnAdunare);
			 panel3.add(btnScadere);
			 panel3.add(btnInmultire);
			 panel3.add(btnImpartire);
	
			 panel4.add(btnDerivare);
			 panel4.add(btnIntegrare);
			 panel4.add(btnReset);
			 
			 tfRezultat1.setPreferredSize(new Dimension(200,24));
			 tfRezultat2.setPreferredSize(new Dimension(200,24));
			 panel5.add(new JLabel("Rezultat: "));
			 panel5.add(tfRezultat1);
			 panel5.add(tfRezultat2);
			 
			 JPanel p = new JPanel();
			 p.add(panel1);
			 p.add(panel2);
			 p.add(panel3);
			 p.add(panel4);
			 p.add(panel5);
			 p.setLayout(new BoxLayout(p, BoxLayout.PAGE_AXIS));
			 frame.setContentPane(p);
			 frame.setVisible(true); 
			 
			 setDefaultCloseOperation(EXIT_ON_CLOSE);
		}
		String getInput1() {
	        return tfPolinom1.getText();
	    }
		String getInput2() {
	        return tfPolinom2.getText();
	    }
		void setOutput1(String rezultat1) {
			tfRezultat1.setText(rezultat1);
		}
		void setOutput2(String rezultat2) {
			tfRezultat2.setText(rezultat2);
		}
		void addAdunaListener(ActionListener sum) {
	        btnAdunare.addActionListener(sum);
	    }
		void addDiferentaListener(ActionListener dif) {
			btnScadere.addActionListener(dif);
		}
		void addInmultesteListener(ActionListener mul) {
			btnInmultire.addActionListener(mul);
		}
		void addImparteListener(ActionListener div) {
			btnImpartire.addActionListener(div);
		}
		void addDerivareListener(ActionListener der) {
			btnDerivare.addActionListener(der);
		}
		void addIntegrareListener(ActionListener integr) {
			btnIntegrare.addActionListener(integr);
		}
		void addResetareListener(ActionListener reset) {
			btnReset.addActionListener(reset);
		}
		void addInregistrareListener1(ActionListener inreg) {
			btnInregistrare1.addActionListener(inreg);
		}
		void addInregistrareListener2(ActionListener inreg) {
			btnInregistrare2.addActionListener(inreg);
		}
		
		JButton getBtnInregistrare1() {
			return this.btnInregistrare1;
		}
		JButton getBtnInregistrare2() {
			return this.btnInregistrare2;
		}
}
