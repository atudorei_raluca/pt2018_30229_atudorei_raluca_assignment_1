package mvc;

import model.Model;
import model.PolinomModel;

public class MVC {
	public static void main(String[] args) {

        Model      model      = new PolinomModel();
        View       view       = new View();
        Controller controller = new Controller(model, view);

        view.setVisible(true);
    }
}
