
package mvc;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import model.Model;
import model.Polinom;

public class Controller {

	private Model model;
	private View view;

	public Controller(Model model, View view) {
		this.model = model;
		this.view = view;
		
		view.addAdunaListener(new AdunaListener());
		view.addDiferentaListener(new DiferentaListener());
		view.addInmultesteListener(new InmultesteListener());
		view.addImparteListener(new ImparteListener());
		view.addDerivareListener(new DerivareListener());
		view.addIntegrareListener(new IntegrareListener());
		view.addResetareListener(new ResetareListener());
		view.addInregistrareListener1(new InregistrareListener1());
		view.addInregistrareListener2(new InregistrareListener2());
	}
	
	class InregistrareListener1 implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			String input = view.getInput1();
			try {
				model.setFirstValue(input);
				view.showDialog("Polinom inregistrat");
			} catch (IOException ex) {
				view.showDialog("Polinom Invalid:" + ex.getMessage());
			}
		}
	}
	class InregistrareListener2 implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			String input = view.getInput2();
			try {
				model.setSecondValue(input);
				view.showDialog("Polinom inregistrat");
			} catch (IOException ex) {
				view.showDialog("Polinom invalid:" + ex.getMessage());
			}
		}
	}

	class AdunaListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			String result = model.sum();
			view.setOutput1(result);
		}
	}

	class DiferentaListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			String result = model.dif();
			view.setOutput1(result);
		}
	}

	class InmultesteListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			String result = model.mult();
			view.setOutput1(result);
		}
	}

	class ImparteListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			String cat = model.subCat();
			String rest= model.subRest();
			view.setOutput1(cat);
			view.setOutput2(rest);
		}
	}

	class DerivareListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
				String result1 = model.derivFirst();
				view.setOutput1(result1);
				String result2 = model.derivSecond();
				view.setOutput2(result2);
			}
	}

	class IntegrareListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			if(view.getBtnInregistrare1().isEnabled()) {
				String result = model.integrFirst();
				view.setOutput1(result);
			}
			if(view.getBtnInregistrare2().isEnabled()) {
				String result = model.integrSecond();
				view.setOutput2(result);
			}
		}
	}
	class ResetareListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			view.setOutput1("");
			view.setOutput2("");
		}
	}
}
