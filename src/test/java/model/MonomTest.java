package model;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.jupiter.api.Test;

class MonomTest {
	private static Monom m1;
	private static Monom m2;
	public MonomTest() {
		
	}
	@Test
	void testAdunaMonom() throws IOException {
		m1= new Monom("2x^5");
		m2= new Monom("4x^5");
		m1.adunaMonom(m2);
		String t= m1.toString();
		assertEquals("+6.0x^5", t);
	}

	@Test
	void testSchimbaSemnul() throws IOException {
		m1= new Monom("2x^5");
		m1.schimbaSemnul();
		String t= m1.toString();
		assertEquals("-2.0x^5", t);
	}

	@Test
	void testInmultesteMonom() throws IOException {
		m1= new Monom("2x^5");
		m2= new Monom("4x^5");
		m1.inmultesteMonom(m2);
		String t= m1.toString();
		assertEquals("+8.0x^10", t);
	}

	@Test
	void testDeriveazaMonom() throws IOException {
		m1= new Monom("2x^5");
		m1.deriveazaMonom();
		String t= m1.toString();
		assertEquals("+10.0x^4", t);
	}

	@Test
	void testIntegreazaMonom() throws IOException {
		m1= new Monom("2x");
		m1.integreazaMonom();
		String t= m1.toString();
		assertEquals("+x^2", t);
	}

	@Test
	void testImparteMonoame() throws IOException {
		m1= new Monom("2x^5");
		m2= new Monom("4x^5");
		Monom m= new Monom(0.0, "x", 0);
		m.imparteMonoame(m2, m1);
		String t= m.toString();
		assertEquals("+2.0", t);
	}

	@Test
	void testInmultesteMonoame() throws IOException {
		m1= new Monom("2x^5");
		m2= new Monom("4x^5");
		Monom m= new Monom(0.0, "x", 0);
		m.inmultesteMonoame(m2, m1);
		String t= m.toString();
		assertEquals("+8.0x^10", t);
	}

}
