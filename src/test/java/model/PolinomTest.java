package model;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;
import org.junit.platform.commons.annotation.Testable;

public class PolinomTest {
	private static Polinom p1;;
	private static Polinom p2;
	public PolinomTest() {
		
	}
	
	@Test
	public void testAdunaPolinom() throws IOException {
		p1= new Polinom("7x^2-5x+2");
		p2= new Polinom("-2x^2-4");
		p1.adunaPolinom(p2);
		String t= p1.toString();
		assertEquals("+5.0x^2-5.0x-2.0",t);
	}
	
	@Test
	public void testScadePolinom() throws IOException {
		p1= new Polinom("7x^2-5x+2");
		p2= new Polinom("-2x^2-4");
		p1.scadePolinom(p2);
		String t= p1.toString();
		assertEquals("+9.0x^2-5.0x+6.0",t);
	}
	
	@Test
	public void testInmultestePolinom() throws IOException {
		p1= new Polinom("7x^2-5x+2");
		p2= new Polinom("-2x^2-4");
		p1.inmultestePolinom(p2);
		String t= p1.toString();
		assertEquals("-14.0x^4+10.0x^3-32.0x^2+20.0x-8.0",t);
	}
	
	@Test
	public void testImpartePolinom() throws IOException {
		p1= new Polinom("x^6-2x^4-x+1");
		p2= new Polinom("x^3-3x^2+x+2");
		p1.impartePolinom(p2);
		String rest= p1.getRest();
		assertEquals("+27.0x^2-26.0x-25.0",rest);
		String cat= p1.getCat();
		assertEquals("+x^3+3.0x^2+6.0x+13.0", cat);
	}
	
	@Test
	public void testDeriveazaPolinom() throws IOException {
		p1= new Polinom("7x^2-5x+2");
		p1.deriveazaPolinom();
		String t= p1.toString();
		assertEquals("+14.0x-5.0",t);
	}
	
	@Test
	public void testIntegreazaPolinom() throws IOException {
		p1= new Polinom("10x^4+3x^2-27");
		p1.integreazaPolinom();
		String t= p1.toString();
		assertEquals("+2.0x^5+x^3-27.0x",t);
	}
	
}
